/**
 * Created by Алина on 15.01.2022.
 */

// Теоретический вопрос
    // 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
    // Для работы с input не рекомендуется использовать события клавиатуры, так как
    // ввод данных в input может происходить не только с помощью клавиатуры, но и при
    // помощи мыши,а также при распознавании речи во время диктовки.

let buttons = document.getElementsByClassName("btn")
document.addEventListener('keyup', function(event) {
    let pressedKey = event.key;
    for (let btn of buttons) {
        btn.setAttribute("data-state", "select")
        if (btn.innerText.toLowerCase() !== pressedKey.toLowerCase()){
            btn.removeAttribute("data-state", "select")
        }
    }
})




